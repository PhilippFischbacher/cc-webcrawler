![pipeline status](https://gitlab.com/PhilippFischbacher/cc-webcrawler/badges/main/pipeline.svg)
![coverage](https://gitlab.com/PhilippFischbacher/cc-webcrawler/badges/main/coverage.svg)

> > # CleanCode Webcrawler
> by Philipp Fischbacher & Peter Miklautz

> # Description
>> The task of this assignment is to develop a Web-Crawler in Java which provides a compact overview of the given website and linked websites in a given language by only listing the headings and the links. The attached example-report.md shows an example on how the overview should look like (feel free to make improvements upon the suggested layout).
>>
>> **Must have features:**
>> * input the URL, the depth of websites to crawl, and the target language as command line arguments
>> * create a compact overview of the crawled websites in a specified target language
>>   * record and translate only the headings
>>   * represent the depth of the crawled websites with proper indentation (see example)
>>   * also record the URLs of the crawled sites
>>   * highlight broken links
>> * find the links to other websites and recursively do the analysis for those websites (it is enough if you analyze the pages at a depth of 2 without visiting further links, you might also allow the user to configure this depth via command line)
>> * store the results in single mark down file (.md extension)
>> * feature to allow concurrent crawling and translation of multiple websites. Given two or more URLs as input to your program your solution should concurrently gather the data for the websites and its referenced websites At the end you should combine the results for each website in a single report while retaining the original structure of the websites.
>> * Implement proper and clean Error Handling. Various errors might occur when crawling and/or translating a website. Protect your application against crashing by handling the errors and informing the user through logging the error in the report.
>> * The dependencies on third-party libraries should be small to allow future updates or even replacements of the third-party libraries. 

> # Testing
>> Tests can be runned with ``gradle test``
>>
>> Coverage report is created when running ``gradle clean test jacocoTestReport`

> # Build JAR
>> As we need to be able to execute the crawler via the command line
>> you can generate a JAR file with ``gradle clean shadowJar``
>>
>> The file is located in ``/build/libs/cc-webcrawler-1.0-SNAPSHOT-all.jar``

> # Execute JAR
>> You can execute the JAR with
>> ``java -jar cc-webcrawler-1.0-SNAPSHOT-all.jar`` to get information how to use the crawler
>>
>>
>> ### Example usage:
>> You can specify 1...n URLs after `-url` to concurrently crawl for them and get a combined markdown result file.
>>
>> ``java -jar cc-webcrawler-1.0-SNAPSHOT-all.jar -url http://histo.io/ http://test.at/ -depth 1 -source EN -target DE``
