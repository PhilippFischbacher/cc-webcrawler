package htmlParser;

import crawler.Crawler;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

public class HtmlParser {
    private ParsedHTMLResult parsedHTMLResult;
    private Document crawledHTMLDocument;

    /**
     * This constructor initializes a HtmlParser, who checks through a HTML document
     * and parses out all the necessary information.
     * In this case specifically: It parses for headings and links.
     * @param crawledHTMLDocument is the HTML document returned from the crawler that
     *                            we now want to parse through.
     */
    public HtmlParser (Document crawledHTMLDocument){
        this.parsedHTMLResult = new ParsedHTMLResult();
        this.crawledHTMLDocument = crawledHTMLDocument;
    }

    /**
     * This method parses through the document that is currently
     * connected to this instance of HtmlParser and returns the result
     * as a ParsedHTMLResult object.
     *
     * First the document is parsed for all the headings h1-h6.
     * The result of this step are saved into the ParsedHTMLResult.
     *
     * Then the document is parsed for all links.
     * The result of this step are saved into the ParsedHTMLResult.
     *
     * Lastly, all the links are checked for whether they are broken or not.
     * The result of this step are saved into the ParsedHTMLResult.
     *
     * @return a ParsedHTMLResult object containing all the information we required.
     */
    public ParsedHTMLResult parseHTMLDocument() {
        this.parseHTMLDocumentForHeadings();
        this.parseHTMLDocumentForLinks();
        this.checkForBrokenLinks();
        return this.parsedHTMLResult;
    }

    private void parseHTMLDocumentForHeadings(){
        this.parsedHTMLResult.setHeadings(
                this.crawledHTMLDocument.select("h1, h2, h3, h4, h5, h6")
        );
    }

    private void parseHTMLDocumentForLinks(){
        this.parsedHTMLResult.setWorkingLinks(
                this.crawledHTMLDocument.select("a[href]")
        );
    }

    private void checkForBrokenLinks() {
        for (Element workingLink: this.parsedHTMLResult.getWorkingLinks()) {
            manageLinksAccordingToAccessibility(workingLink);
        }
        this.parsedHTMLResult.getWorkingLinks().removeAll(this.parsedHTMLResult.getBrokenLinks());
    }

    private void manageLinksAccordingToAccessibility(Element linkToCheck) {
        boolean isUrlBroken = Crawler.isUrlBroken(linkToCheck.attr("href"));
        if(isUrlBroken) {
            this.parsedHTMLResult.getBrokenLinks().add(linkToCheck);
        }
    }

}
