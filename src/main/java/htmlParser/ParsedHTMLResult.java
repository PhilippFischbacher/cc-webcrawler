package htmlParser;

import org.jsoup.select.Elements;

public class ParsedHTMLResult {
    private Elements headings = new Elements();
    private Elements workingLinks = new Elements();
    private Elements brokenLinks = new Elements();

    private int currentDepth;

    private String crawledURLInformation;

    public Elements getHeadings() {
        return headings;
    }

    public void setHeadings(Elements headings) {
        this.headings = headings;
    }

    public Elements getWorkingLinks() {
        return workingLinks;
    }

    public void setWorkingLinks(Elements workingLinks) {
        this.workingLinks = workingLinks;
    }

    public Elements getBrokenLinks() {
        return brokenLinks;
    }

    public void setBrokenLinks(Elements brokenLinks) {
        this.brokenLinks = brokenLinks;
    }

    public int getCurrentDepth() {
        return currentDepth;
    }

    public void setCurrentDepth(int currentDepth) {
        this.currentDepth = currentDepth;
    }

    public String getCrawledURLInformation() {
        return crawledURLInformation;
    }

    public void setCrawledURLInformation(String crawledURLInformation) {
        this.crawledURLInformation = crawledURLInformation;
    }
}
