package crawler;

import org.jsoup.nodes.Document;
import wrappers.JsoupWrapper;

import java.io.IOException;

public class Crawler {
    private String url;

    /**
     * Constructor to initialize Crawler.
     * @param url URL that we want to crawl.
     */
    public Crawler(String url) {
        this.url = url;
    }

    /**
     * This method uses whatever crawler library we're using to crawl through
     * the URL of the current crawler-object and returns the HTML document.
     * @return The HTML document containing all the page information of the crawled URL.
     */
    public Document getHTMLData() {
        try {
            Document htmlPage = JsoupWrapper.getHTMLDocument(this.url);
            return htmlPage;
        } catch (IOException e) {
            return null;
        }
    }

    /**
     * This static method uses whatever crawler library we're using to check whether
     * the URL in the parameter is broken or not.
     * @param url URL that we want to check.
     * @return  true if the URL is broken
     *          false if the URL is not broken
     */
    public static boolean isUrlBroken(String url) {
        return JsoupWrapper.isUrlNotCallable(url);
    }
}
