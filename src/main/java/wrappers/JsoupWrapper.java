package wrappers;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

public class JsoupWrapper {

    public static Document getHTMLDocument(String url) throws IOException {
        return Jsoup.connect(url).get();
    }

    public static boolean isUrlNotCallable(String url) {
        try {
            Connection.Response response =  Jsoup.connect(url)
                    .ignoreContentType(true)
                    .timeout(500)
                    .execute();
            return false;
        } catch (Exception brokenLinkException) {
            return true;
        }
    }
}
