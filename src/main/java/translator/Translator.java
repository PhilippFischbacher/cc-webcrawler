package translator;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Translator {
    private static final String AUTH_KEY = "fba0bcf5-a0b4-b6c5-8d5d-cafb0d81fad5:fx";
    private static final String API_ENDPOINT = "https://api-free.deepl.com/v2/translate";
    private static final String TRANSLATION_FAILED = "(Translation FAILED)";

    private static String toTranslate;
    private static String sourceLanguage;
    private static String targetLanguage;
    private static String translated;
    private static String responseBody;
    private static boolean isFailedRequest = false;


    public static void setTranslatorParameters(String toTranslateInput, String sourceLanguageInput, String targetLanguageInput) throws IOException {

        toTranslate = toTranslateInput;
        sourceLanguage = sourceLanguageInput;
        targetLanguage = targetLanguageInput;
        responseBody = "";
    }

    /**
     * Method that initializes the translation-process
     * returns the same string in case any exceptions occur during translation
     * @return the translated string
     */
    public static String translate(){
        URL deepl = setupTranslationURL();
        if(deepl != null
        && managedConnectionToTranslator(deepl)) {
            return translated;
        }
        translated=toTranslate + TRANSLATION_FAILED;
        return translated;
    }

    private static URL setupTranslationURL(){
        String translateURLAsString = buildTranslateURL();
        try {
            URL translationURL = new URL(translateURLAsString);
            return translationURL;
        }catch(MalformedURLException malformedURLException){
            return null;
        }
    }

    private static boolean managedConnectionToTranslator(URL deepl){
        try {
            HttpURLConnection httpConnection = initializeHTTPConnection(deepl);

            getTranslationFromDeeplWithInitializedConnection(httpConnection);

            httpConnection.disconnect();

            return true;
        }catch (IOException ioException){
            return false;
        }
    }

    private static void getTranslationFromDeeplWithInitializedConnection(HttpURLConnection httpConnection) throws IOException{
        getHTTPResponseBody(httpConnection);
        parseDeeplResponse();
    }

    private static HttpURLConnection initializeHTTPConnection(URL deepl) throws IOException {
        HttpURLConnection httpConnection = (HttpURLConnection) deepl.openConnection();
        httpConnection.setRequestMethod("POST");
        httpConnection.setRequestProperty("Content-Length", "0");
        httpConnection.setDoOutput(true);
        return httpConnection;
    }


    private static void getHTTPResponseBody(HttpURLConnection httpConnection) throws IOException {
        BufferedReader br = null;
        if (100 <= httpConnection.getResponseCode() && httpConnection.getResponseCode() <= 399) {
            br = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
        }else {
            br = new BufferedReader(new InputStreamReader(httpConnection.getErrorStream()));
            isFailedRequest = true;
        }

        String currentLine;
        while ((currentLine = br.readLine()) != null) {
            responseBody += currentLine;
        }
    }

    private static void parseDeeplResponse() {
        if(!isFailedRequest) {
            JSONObject deeplResponse = new JSONObject(responseBody);
            JSONArray translations = deeplResponse.getJSONArray("translations");
            translated = (String) translations.getJSONObject(0).get("text");
        } else {
            translated = toTranslate + TRANSLATION_FAILED;
        }
    }

    private static String buildTranslateURL() {
        StringBuilder translateURL = new StringBuilder();
        translateURL
                .append(API_ENDPOINT)
                .append("?auth_key=").append(AUTH_KEY)
                .append("&text=").append(toTranslate)
                .append("&target_lang=").append(targetLanguage)
                .append("&source_lang=").append(sourceLanguage);

        return translateURL.toString().replace(" ", "%20");
    }
}
