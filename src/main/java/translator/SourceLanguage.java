package translator;

public enum SourceLanguage {

    BG ("BG"),
    CD ("CS"),
    DA ("DA"),
    DE ("DE"),
    EL ("EL"),
    EN ("EN"),
    ES ("ES"),
    ET ("ET"),
    FI ("FI"),
    FR ("FR"),
    HU ("HU"),
    IT ("IT"),
    JA ("JA"),
    LT ("LT"),
    LV ("LV"),
    NL ("NL"),
    PL ("PL"),
    PT ("PT"),
    RO ("RO"),
    RU ("RU"),
    SK ("SK"),
    SL ("SL"),
    SV ("SV"),
    ZH ("ZH");


    private final String sourceLanguage;
    
    private SourceLanguage(String language) {
        sourceLanguage=language;
    }

    public static boolean isValidSourceLanguage(String languageInput){

        SourceLanguage sourceLanguageTest = SourceLanguage.valueOf(languageInput);

        if(sourceLanguageTest.sourceLanguage==null){
            return false;
        }
        return true;
    }

}
