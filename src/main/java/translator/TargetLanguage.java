package translator;

public enum TargetLanguage {

    BG ("BG"),
    CS ("CS"),
    DA ("DA"),
    DE ("DE"),
    EL ("EL"),
    EN ("EN"),
    ES ("ES"),
    ET ("ET"),
    FI ("FI"),
    FR ("FR"),
    HU ("HU"),
    IT ("IT"),
    JA ("JA"),
    LT ("LT"),
    LV ("LV"),
    NL ("NL"),
    PL ("PL"),
    PT ("PT"),
    RO ("RO"),
    RU ("RU"),
    SK ("SK"),
    SL ("SL"),
    SV ("SV"),
    ZH ("ZH");

    private final String targetLanguage;

    private TargetLanguage(String language) {
        targetLanguage=language;
    }

    public static boolean isValidTargetLanguage(String languageInput){
        TargetLanguage targetLanguageTest = TargetLanguage.valueOf(languageInput);

        if(targetLanguageTest.targetLanguage==null){
            return false;
        }
        return true;
    }

}
