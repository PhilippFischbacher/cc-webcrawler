package dateFormatter;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatter {

    /**
     * This method calculates the current date and returns it
     * in the required format.
     * @return a String containing the current date.
     */
    public static String getCurrentDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy_HHmmss");
        Date date = new Date();
        return formatter.format(date);
    }
}
