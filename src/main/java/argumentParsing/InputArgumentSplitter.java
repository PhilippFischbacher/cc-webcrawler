package argumentParsing;

public class InputArgumentSplitter {
    private static final int NUMBER_OF_NON_URL_ARGUMENTS = 7;
    private static String[] arguments;
    private static int numberOfTotalArguments;
    private static int numberOfUrls;

    private static void initializeInputArgumentSplitter(String[] args) {
        arguments = args;
    }

    public static String[] splitArguments(String[] args) {
        initializeInputArgumentSplitter(args);
        numberOfTotalArguments = arguments.length;
        numberOfUrls = numberOfTotalArguments-NUMBER_OF_NON_URL_ARGUMENTS;
        String[] singleUrlArguments = new String[NUMBER_OF_NON_URL_ARGUMENTS+1];
        singleUrlArguments[0]=arguments[0];

        for(int i=numberOfUrls+1, j=2; i<numberOfTotalArguments; i++, j++){
            singleUrlArguments[j]=arguments[i];
        }
        return singleUrlArguments;
    }

    public static int getNumberOfUrls() {
        return numberOfUrls;
    }
}
