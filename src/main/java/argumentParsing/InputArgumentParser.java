package argumentParsing;

import crawler.Crawler;
import translator.SourceLanguage;
import translator.TargetLanguage;

public class InputArgumentParser {
    private String[] arguments;
    private ParsedInputArguments parsedArguments;

    public InputArgumentParser(String[] args) {
        this.arguments = args;
        this.parsedArguments = new ParsedInputArguments();
    }

    /**
     * This method takes a String array as input and compares it to the program-calling-conventions.
     * If the argument array fits all the requirements and conventions, the information required
     * for the crawler and the translator, a ParsedInputArguments object is filled with all the
     * relevant data and then returned.
     * @return A ParsedInputArguments object, containing only the necessary information required
     * for the crawler and translator to work.
     */
    public ParsedInputArguments parseInputArguments() {
        try {
            areNumberOfArgumentsValid();
            setURLFromInputArgument();
            setDepthFromInputArgument();
            setSourceLanguageFromInputArgument();
            setTargetLanguageFromInputArgument();
            return parsedArguments;
        }catch (IllegalArgumentException e){
            throw e;
        }
    }

    private void setURLFromInputArgument() throws IllegalArgumentException{
        isURLArgumentMissing();
        isURLArgumentValid();
        parsedArguments.setURL(this.arguments[1]);
    }

    private void setDepthFromInputArgument() throws IllegalArgumentException{
        isDepthArgumentValid();
        parsedArguments.setDepth(Integer.parseInt(this.arguments[3]));
    }

    private void setSourceLanguageFromInputArgument() throws IllegalArgumentException{
        isSourceLanguageArgumentValid();
        parsedArguments.setSourceLanguage(this.arguments[5]);
    }

    private void setTargetLanguageFromInputArgument() throws IllegalArgumentException{
        isTargetLanguageArgumentValid();
        parsedArguments.setTargetLanguage(this.arguments[7]);
    }

    private void isURLArgumentMissing() throws IllegalArgumentException {
        if(!this.arguments[0].equals("-url") || this.arguments[1].startsWith("-")){
            throw new IllegalArgumentException("Url Input argument is missing");
        }
    }

    private void isURLArgumentValid() throws IllegalArgumentException {
        if(Crawler.isUrlBroken(this.arguments[1])){
            throw new IllegalArgumentException("Url Input argument is broken!");
        }
    }

    private void isDepthArgumentValid() throws IllegalArgumentException {
        if(!this.arguments[2].equals("-depth") || this.arguments[3].startsWith("-")){
            throw new IllegalArgumentException("Depth Input argument is missing or invalid");
        }
        try{
            int parseIntTest = Integer.parseInt(this.arguments[3]);
        }catch (NumberFormatException nfe){
            throw new IllegalArgumentException("Please enter an integer number as depth input");
        }
    }

    private void isSourceLanguageArgumentValid() throws IllegalArgumentException {
        if(!this.arguments[4].equals("-source") || this.arguments[5].startsWith("-")){
            throw new IllegalArgumentException("Source Language Input tag is missing or invalid");
        }
        try{
            SourceLanguage.isValidSourceLanguage(this.arguments[5]);
        }catch (IllegalArgumentException iae){
            throw new IllegalArgumentException("Source Language Input argument doesn't follow Deepl language conventions");
        }
    }

    private void isTargetLanguageArgumentValid() throws IllegalArgumentException {
        if(!this.arguments[6].equals("-target") || this.arguments[7].startsWith("-")){
            throw new IllegalArgumentException("Target Language Input tag is missing or invalid");
        }
        try{
            TargetLanguage.isValidTargetLanguage(this.arguments[7]);
        }catch (IllegalArgumentException iae){
            throw new IllegalArgumentException("Target Language Input argument doesn't follow Deepl language conventions");
        }
    }

    private void areNumberOfArgumentsValid() throws IllegalArgumentException {
        if(this.arguments.length!=8){
            throw new IllegalArgumentException
                    ("The number of arguments is invalid\n" +
                            "Please make sure to use this format:\n" +
                            "-url <URL> -depth <depth how far the program crawls> -source <headings' source language> -target <target language for the headings>");
        }
    }
}
