package markdownGenerator;

import htmlParser.ParsedHTMLResult;
import org.jsoup.nodes.Element;
import translator.Translator;

import java.io.IOException;
import java.util.ArrayList;

public class MarkdownGenerator {
    private StringBuilder markdownString;
    private ArrayList<ParsedHTMLResult> parsedHTMLResults;
    private String crawledURL;
    private Integer crawlDepth;
    private String sourceLanguage;
    private String targetLanguage;
    private Integer currentDepth;
    private Integer currentHTMLResult;
    private Boolean useTranslator = false;

    public MarkdownGenerator(ArrayList<ParsedHTMLResult> parsedHTMLResults, String crawledURL, Integer crawlDepth,
                             String sourceLanguage, String targetLanguage, Boolean useTranslator) {
        this.markdownString = new StringBuilder();
        this.parsedHTMLResults = parsedHTMLResults;
        this.crawledURL = crawledURL;
        this.crawlDepth = crawlDepth;
        this.sourceLanguage = sourceLanguage;
        this.targetLanguage = targetLanguage;
        this.useTranslator = useTranslator;
    }

    /**
     * This method generates the string we then want to save with MarkdownFile.
     * For this, it uses the markdownString StringBuilder variable initialized in the constructor.
     *
     * First it appends general information about the crawling-process, like what the original
     * input URL was and how deep the generated crawling-tree is.
     * @return a String containing all the crawling results in the specified format that can
     *          then be used to generate a file.
     * @throws IOException
     */
    public String generateMarkdownString() throws IOException {
        appendCrawlingInformation();
        appendTranslatorInformation();
        appendParsedHTMLResults();
        return markdownString.toString();
    }

    private void appendCrawlingInformation() {
        this.markdownString.append("input: <a>").append(this.crawledURL).append("</a>\n");
        this.markdownString.append("<br>depth: ").append(this.crawlDepth).append("\n");
    }

    private void appendTranslatorInformation() {
        this.markdownString.append("<br>source language: ").append(this.sourceLanguage).append("\n");
        this.markdownString.append("<br>target language: ").append(this.targetLanguage).append("\n\n");
    }

    private void appendParsedHTMLResults() throws IOException {
        for(ParsedHTMLResult parsedHTMLResult: this.parsedHTMLResults) {
            this.currentHTMLResult = this.parsedHTMLResults.indexOf(parsedHTMLResult);
            this.currentDepth = parsedHTMLResult.getCurrentDepth();
            appendSingularParsedHTMLResult();
        }
    }

    private void appendSingularParsedHTMLResult() throws IOException {
        appendCrawledURLInformation();
        appendHeadings();
        appendBrokenLinks();
        appendWorkingLinks();
    }

    private void appendCrawledURLInformation() {
        this.markdownString
                .append("\n-CRAWLED FROM: ")
                .append(this.parsedHTMLResults.get(this.currentHTMLResult).getCrawledURLInformation())
                .append("\n");
    }

    private void appendHeadings() throws IOException {
        for(Element heading: this.parsedHTMLResults.get(currentHTMLResult).getHeadings()) {
            appendHeading(heading);
        }
    }

    private void appendBrokenLinks() {
        for(Element brokenLink: this.parsedHTMLResults.get(currentHTMLResult).getBrokenLinks()) {
            appendBrokenLink(brokenLink);
        }
    }

    private void appendWorkingLinks() {
        for(Element workingLink: this.parsedHTMLResults.get(currentHTMLResult).getWorkingLinks()) {
            appendWorkingLink(workingLink);
        }
    }

    private void appendHeading(Element heading) throws IOException {
        appendMarkdownHeading(heading);
        appendDepthIndicatorArrows();
        this.markdownString.append(getHeadingText(heading)).append("\n");
    }

    private String getHeadingText(Element heading) throws IOException {
        String headingText;
        if(this.useTranslator) {
            headingText = getTranslatedHeading(heading.text());
        } else {
            headingText = heading.text();
        }
        return headingText;
    }

    private void appendBrokenLink(Element brokenLink){
        appendLink(brokenLink, true);
    }

    private void appendWorkingLink(Element workingLink){
        appendLink(workingLink, false);
    }

    private void appendMarkdownHeading(Element heading){
        String markdownHeading = MarkdownHeadings.valueOf(heading.tagName()).markDownHeadingType;
        this.markdownString.append(markdownHeading).append(" ");
    }

    private void appendDepthIndicatorArrows(){
        this.markdownString.append(getAppropriateArrowSize(this.currentDepth).toString()).append(" ");
    }

    private String getTranslatedHeading(String headingToTranslate) throws IOException {
        Translator.setTranslatorParameters(headingToTranslate, sourceLanguage, targetLanguage);
        return Translator.translate();
    }

    private void appendLink(Element link, boolean isBroken) {
        this.markdownString.append("<br> ");
        appendDepthIndicatorArrows();
        if(isBroken) {
            this.markdownString.append("broken ");
        }
        this.markdownString.append("link <a>").append(link.attr("href")).append("</a>\n");
    }

    private StringBuilder getAppropriateArrowSize(int currentDepth){
       StringBuilder arrow = new StringBuilder();
       for (int i=0; i<currentDepth; i++){
           arrow.append("--");
       }
       arrow.append(">");
       return arrow;
    }

}
