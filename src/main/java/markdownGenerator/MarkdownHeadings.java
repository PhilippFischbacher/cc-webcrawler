package markdownGenerator;

public enum MarkdownHeadings {
    h1("#"),
    h2("##"),
    h3("###"),
    h4("####"),
    h5("#####"),
    h6("######");

    public final String markDownHeadingType;

    private MarkdownHeadings(String markDownHeadingType) {
        this.markDownHeadingType = markDownHeadingType;
    }
}
