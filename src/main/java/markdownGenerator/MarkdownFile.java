package markdownGenerator;

import dateFormatter.DateFormatter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class MarkdownFile {
    private static final String MARKDOWN_FILE_PREFIX = "CCWebcrawler-Output-";
    private static final String MARKDOWN_FILE_SUFFIX = ".md";
    private static String generatedFileName;

    /**
     * This method initializes our MarkdownFile.
     */
    private static void createMarkdownFile() {
        generatedFileName = MARKDOWN_FILE_PREFIX + DateFormatter.getCurrentDate() + MARKDOWN_FILE_SUFFIX;
        try {
            File newMarkdownFile = new File(generatedFileName);
            if (newMarkdownFile.createNewFile()) {
                System.out.println("File created: " + newMarkdownFile.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            System.out.println("An error occurred when creating the markdown File.");
            e.printStackTrace();
        }
    }

    /**
     * This method first calls createMarkdownFile to initialize and create a file
     * and then writes the string that is submitted as method parameter into
     * said MarkdownFile.
     * @param markdownInput is the String we want to save into our markdown file
     */
    public static void saveMarkdownFile(String markdownInput) {
        createMarkdownFile();
        try {
            FileWriter myWriter = new FileWriter(generatedFileName);
            myWriter.write(markdownInput);
            myWriter.close();
            System.out.println("Successfully saved the Markdown file.");
        } catch (IOException e) {
            System.out.println("An error occurred when writing to the markdown File.");
            e.printStackTrace();
        }
    }
}
