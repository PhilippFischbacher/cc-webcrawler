package execution;

import argumentParsing.InputArgumentParser;
import argumentParsing.InputArgumentSplitter;
import argumentParsing.ParsedInputArguments;
import crawler.Crawler;
import htmlParser.HtmlParser;
import htmlParser.ParsedHTMLResult;
import markdownGenerator.MarkdownFile;
import markdownGenerator.MarkdownGenerator;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import translator.Translator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class CrawlerExecution {
    private static CopyOnWriteArrayList<String> markdownsToPrint = new CopyOnWriteArrayList<String>();
    private static ArrayList<Thread> crawlingThreads = new ArrayList<>();

    /**
     * @param args -url www.test.com -depth 1 -source EN -target DE
     */
    public static void main(String[] args) throws IOException {
        String[] singleUrlArguments = InputArgumentSplitter.splitArguments(args);

        for (int i=0; i<InputArgumentSplitter.getNumberOfUrls(); i++) {
            startSingleURLCrawlingThread(args, singleUrlArguments, i);
        }

        int currentlyRunningThreads = 0;
        do {
            currentlyRunningThreads = countCurrentRunningThreads();
        } while (currentlyRunningThreads > 0);
        System.out.println("Crawling for all sites has finished!");

        generateMergedMarkdownOutputFile();
    }

    public static synchronized void addGeneratedMarkdownString(String generatedMarkdown) {
        markdownsToPrint.add(generatedMarkdown);
    }

    private static void startSingleURLCrawlingThread(String[] args, String[] singleUrlArguments, int i) {
        singleUrlArguments[1]= args[i +1];
        String[] threadURLArguments = singleUrlArguments.clone();
        Runnable crawlingTask = new CrawlRunner(threadURLArguments);
        Thread currentUrlWorker = new Thread(crawlingTask);

        currentUrlWorker.start();
        crawlingThreads.add(currentUrlWorker);
    }

    private static int countCurrentRunningThreads() {
        int currentlyRunningThreads = 0;
        for (Thread t: crawlingThreads) {
            if(t.isAlive()) {
                currentlyRunningThreads++;
            }
        }
        return currentlyRunningThreads;
    }

    private static void generateMergedMarkdownOutputFile() {
        StringBuilder output = new StringBuilder();
        for (int i = 0; i<=markdownsToPrint.size()-1; i++) {
            output.append(markdownsToPrint.get(i)).append("\n\n\n");
        }
        MarkdownFile.saveMarkdownFile(output.toString());
    }
}
