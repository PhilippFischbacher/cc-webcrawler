package execution;

import argumentParsing.InputArgumentParser;
import argumentParsing.ParsedInputArguments;
import crawler.Crawler;
import htmlParser.HtmlParser;
import htmlParser.ParsedHTMLResult;
import markdownGenerator.MarkdownFile;
import markdownGenerator.MarkdownGenerator;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.ArrayList;

public class CrawlRunner implements Runnable {
    private final String[] args;
    private Crawler crawler;
    private HtmlParser parser;
    private ArrayList<ParsedHTMLResult> parsedHTMLResults=new ArrayList<>();
    private ParsedInputArguments inputArguments;

    /**
     * Constructor for initializing a crawling-process
     * @param args A string array of arguments for the
     *             crawling process.
     */
    public CrawlRunner(String[] args) {
        this.args = args;
    }

    /**
     * This method overwrites the default run method and will be called by our threads.
     * The arguments that were already parsed to the CrawlRunner in the constructor are
     * parsed with the InputArgumentParser to return the ParsedInputArgument object containing
     * all the important information for the crawler and translator.
     *
     * Then the generateHTMLResult method is called to recursively crawl through the selected URL
     * and the valid URLS within it up to the determined depth.
     *
     *Afterwards, a MarkdownGenerator is initialized to print the results into a MarkdownFile.
     */
    @Override
    public void run() {
        try {
            InputArgumentParser inputArgumentParser = new InputArgumentParser(args);
            inputArguments = inputArgumentParser.parseInputArguments();
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            return;
        }
        generateHTMLResults(0, inputArguments.getURL());
        MarkdownGenerator mdGenerator = new MarkdownGenerator(
                parsedHTMLResults, inputArguments.getURL(), inputArguments.getDepth(), inputArguments.getSourceLanguage(), inputArguments.getTargetLanguage(), true
        );
        String markdownOutput = null;
        try {
            markdownOutput = mdGenerator.generateMarkdownString();
            CrawlerExecution.addGeneratedMarkdownString(markdownOutput);
        } catch (IOException e) {
            //TODO: Handle Error
            e.printStackTrace();
        }
    }

    /**
     * Recursive method for our crawling-tree.
     * Until the depth mentioned in the inputArguments is reached, this will crawl through
     * the submitted url, note all the required information in a ParsedHTMLResult file and
     * add it to the total parsedHTMLResults (instance variable).
     * Then, the method proceeds to call itself for every non-broken URL that was just
     * acquired from that page.
     * @param currentDepth represents the current depth of our crawling-tree.
     * @param URL is the URL of the page we currently want to crawl.
     */
    private void generateHTMLResults(int currentDepth, String URL){
        if(currentDepth > inputArguments.getDepth()){
            return;
        }

        crawler = new Crawler(URL);
        Document crawledWebsite = crawler.getHTMLData();
        parser = new HtmlParser(crawledWebsite);
        ParsedHTMLResult result = parser.parseHTMLDocument();
        result.setCurrentDepth(currentDepth);
        result.setCrawledURLInformation(URL);
        parsedHTMLResults.add(result);
        for(Element link : result.getWorkingLinks()) {
            generateHTMLResults(currentDepth+1, link.attr("href"));
        }
    }
}
