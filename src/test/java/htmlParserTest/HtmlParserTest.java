package htmlParserTest;

import crawler.Crawler;
import htmlParser.HtmlParser;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
public class HtmlParserTest {
    private HtmlParser htmlParser;
    private final String baseUrl = "http://somesite.com/";
    private Document crawledHTMLDocument;

    private static MockedStatic<Crawler> crawlerStaticMock;

    @BeforeAll
    private static void initStaticMocks() {
        crawlerStaticMock = Mockito.mockStatic(Crawler.class);
    }

    @BeforeEach
    public void initializeParserTest(){
        crawledHTMLDocument = createEmptyTestDocument();
        appendHeadingToTestDocument();
        appendLinkToTestDocument();
        this.htmlParser = new HtmlParser(crawledHTMLDocument);
    }

    private Document createEmptyTestDocument() {
        Document testDocument = Document.createShell(this.baseUrl);
        return testDocument;
    }

    private void appendHeadingToTestDocument() {
        Element heading1 = this.crawledHTMLDocument.appendElement("h1");
        heading1.appendText("Heading1");
        Element heading2 = this.crawledHTMLDocument.appendElement("h2");
        heading2.appendText("Heading2");
        Element heading3 = this.crawledHTMLDocument.appendElement("h3");
        heading3.appendText("Heading3");
    }

    private void appendLinkToTestDocument() {
        Element link = this.crawledHTMLDocument.appendElement("a");
        link.appendText("testLink");
        link.attr("href", baseUrl);
    }

    private void initializeValidLink() {
        Mockito.when(Crawler.isUrlBroken(this.baseUrl)).thenReturn(false);
    }

    private void initializeBrokenLink() {
        Mockito.when(Crawler.isUrlBroken(this.baseUrl)).thenReturn(true);
    }

    @Test
    public void testParsedNumberOfHeadings() {
        Elements parsedHeadings = this.htmlParser.parseHTMLDocument().getHeadings();
        Assertions.assertEquals(3, parsedHeadings.size());
    }

    @Test
    public void testParsedNumberOfLinks() {
        initializeValidLink();
        Elements parsedLinks = this.htmlParser.parseHTMLDocument().getWorkingLinks();
        Assertions.assertEquals(1, parsedLinks.size());
    }

    @Test
    public void testParsedLinkHasHrefAttribute() {
        initializeValidLink();
        Elements parsedLinks = this.htmlParser.parseHTMLDocument().getWorkingLinks();
        Assertions.assertTrue(parsedLinks.get(0).hasAttr("href"));
    }

    @Test
    public void testParsedBrokenLink() {
        initializeBrokenLink();
        Elements brokenLinks = this.htmlParser.parseHTMLDocument().getBrokenLinks();
        Assertions.assertEquals(1, brokenLinks.size());
    }

    @AfterAll
    private static void clearStaticMocks() {
        crawlerStaticMock.close();
    }
}
