package testDataResourcesTest;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class TestDataResourceTest {
    private final String GENERAL_TEST_DATA = "This testData should be read with an InputStream in our test classes.";

    @Test
    public void testReadTestDataFileWithInputStream() throws IOException {
        InputStream inputStream = this.getClass().getResourceAsStream("/generalTestData");
        String text = new String(inputStream.readAllBytes(), StandardCharsets.UTF_8);
        Assertions.assertNotNull(inputStream);
        Assertions.assertEquals(GENERAL_TEST_DATA, text);
    }
}
