package translatorTest;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import translator.Translator;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

@PrepareForTest({ URL.class, Translator.class })
public class TranslatorTest {

    public void initializeDeeplMock() throws Exception {
        String url = "https://api-free.deepl.com/v2/translate";
        URL mockedURL = PowerMockito.mock(URL.class);

        PowerMockito.whenNew(URL.class).withArguments(url).thenReturn(mockedURL);
        HttpURLConnection huc = PowerMockito.mock(HttpURLConnection.class);
        InputStream inputStream = new ByteArrayInputStream("Hallo, World".getBytes());
        PowerMockito.when(mockedURL.openConnection()).thenReturn(huc);
        PowerMockito.when(huc.getInputStream()).thenReturn(inputStream);
        PowerMockito.when(huc.getResponseCode()).thenReturn(200);

    }


    @Test
    @Disabled
    /**
     * This test is currently disabled as we face issues with JUnit5 and PowerMockito.
     * Currently it is not possible to mock the URL class with this setup.
     */
    public void testTranslationEnglishToGerman() throws Exception {
        initializeDeeplMock();

        Translator.setTranslatorParameters("Hello, this is a Test.", "EN", "DE");
        String translated = Translator.translate();
        Assertions.assertEquals("Hallo", translated);
    }
}
