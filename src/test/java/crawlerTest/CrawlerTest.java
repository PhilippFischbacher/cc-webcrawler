package crawlerTest;

import crawler.Crawler;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.helper.HttpConnection;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import java.io.IOException;

@ExtendWith(MockitoExtension.class)
public class CrawlerTest {
    private final String baseUrl = "http://somesite.com/";
    private final String brokenUrl = "http://broken.com/";

    private Crawler crawler;
    private Document HTMLReturnForMockedJsoup;

    @Mock
    private Jsoup jsoup;
    @Mock
    private HttpConnection urlConnection;
    @Mock
    private Connection.Response connectionResponse;

    private static MockedStatic<Jsoup> jsoupStaticMock;

    @BeforeAll
    private static void initStaticMocks() {
        jsoupStaticMock = Mockito.mockStatic(Jsoup.class);
    }

    private void initializeBaseTestSetup() throws IOException {
        crawler = new Crawler(this.baseUrl);

        initializeBaseConnection();
        initializeHttpConnectionMock();
    }

    private void initializeBaseConnection() {
        Mockito.when(jsoup.connect(this.baseUrl)).thenReturn(urlConnection);
    }

    private void initializeFailingConnection() {
        Mockito.when(jsoup.connect(this.brokenUrl)).thenThrow(IllegalArgumentException.class);
    }

    private void initializeHttpConnectionMock() throws IOException {
        HTMLReturnForMockedJsoup = createEmptyTestDocument();
        Mockito.lenient().when(urlConnection.get()).thenReturn(HTMLReturnForMockedJsoup);
    }

    private Document createEmptyTestDocument() {
        Document testDocument = Document.createShell(this.baseUrl);
        return testDocument;
    }

    private void appendHeadingToTestDocument() {
        Element heading = this.HTMLReturnForMockedJsoup.appendElement("h1");
        heading.appendText("Heading1");
    }

    private void initializeFailingHttpConnectionGet() throws IOException {
        Mockito.lenient().when(urlConnection.get()).thenThrow(IOException.class);
    }

    @Test
    public void testHTMLWithExistingBody() throws IOException {
        initializeBaseTestSetup();
        Document testPageHTML = this.crawler.getHTMLData();
        Assertions.assertEquals(1, testPageHTML.getElementsByTag("body").size());
    }

    @Test
    public void testGetHTMLDataException() throws IOException {
        initializeBaseTestSetup();
        initializeFailingHttpConnectionGet();
        Document failedHTML = this.crawler.getHTMLData();
        Assertions.assertNull(failedHTML);
    }

    @Test
    public void testHTMLWithSpecificHeading() throws IOException {
        initializeBaseTestSetup();
        appendHeadingToTestDocument();
        Document testPageHTML = this.crawler.getHTMLData();
        Assertions.assertEquals("Heading1", testPageHTML.getElementsByTag("h1").text());
    }

    @Test
    public void testFailingConnection() {
        initializeFailingConnection();
        boolean isBrokenUrl = Crawler.isUrlBroken(this.brokenUrl);
        Assertions.assertTrue(isBrokenUrl);
    }

    private void initializeValidResponse() throws IOException {
        Mockito.when(jsoup.connect(this.baseUrl)).thenReturn(urlConnection);
        Mockito.when(urlConnection.ignoreContentType(true)).thenReturn(urlConnection);
        Mockito.when(urlConnection.timeout(500)).thenReturn(urlConnection);
        Mockito.when(urlConnection.execute()).thenReturn(connectionResponse);
    }

    @Test
    public void testValidConnection() throws IOException {
        initializeValidResponse();
        boolean isBrokenUrl = Crawler.isUrlBroken(this.baseUrl);
        Assertions.assertFalse(isBrokenUrl);
    }

    @AfterAll
    private static void clearStaticMocks() {
        jsoupStaticMock.close();
    }
}
