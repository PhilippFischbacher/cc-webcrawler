package markdownGeneratorTest;

import htmlParser.ParsedHTMLResult;
import markdownGenerator.MarkdownGenerator;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;


public class MarkdownGeneratorTest {
    private static final String TEST_DATA_DIRECTORY = "/markdownGeneratorTestData/";
    private static final String CRAWLED_URL = "TestURL.com";
    private static final Integer CRAWL_DEPTH = 1;
    private static final String SOURCE_LANGUAGE = "german";
    private static final String TARGET_LANGUAGE = "english";
    private static final Boolean USE_TRANSLATOR = Boolean.FALSE;

    private MarkdownGenerator markdownGenerator;

    private void initializeEmptyMarkdownGenerator() {
        this.markdownGenerator = new MarkdownGenerator(
                new ArrayList<>(),
                "",
                null,
                null,
                null,
                USE_TRANSLATOR
        );
    }

    private void initializeMarkdownGeneratorWithHeadingsOnly() {
        ArrayList<ParsedHTMLResult> parsedHTMLResults = new ArrayList<>();
        ParsedHTMLResult resultOnlyWithHeadings = new ParsedHTMLResult();
        resultOnlyWithHeadings.setHeadings(initializeHeadings());
        parsedHTMLResults.add(resultOnlyWithHeadings);
        this.markdownGenerator = new MarkdownGenerator(
                parsedHTMLResults,
                CRAWLED_URL,
                CRAWL_DEPTH,
                SOURCE_LANGUAGE,
                TARGET_LANGUAGE,
                USE_TRANSLATOR
        );
    }

    private void initializeMarkdownGeneratorWithLinksOnly() {
        ArrayList<ParsedHTMLResult> parsedHTMLResults = new ArrayList<>();
        ParsedHTMLResult resultOnlyWithLinks = new ParsedHTMLResult();
        resultOnlyWithLinks.setWorkingLinks(initializeLinks());
        parsedHTMLResults.add(resultOnlyWithLinks);
        this.markdownGenerator = new MarkdownGenerator(
                parsedHTMLResults,
                CRAWLED_URL,
                CRAWL_DEPTH,
                SOURCE_LANGUAGE,
                TARGET_LANGUAGE,
                USE_TRANSLATOR
        );
    }

    private void initializeMarkdownGeneratorWithBrokenLinksOnly() {
        ArrayList<ParsedHTMLResult> parsedHTMLResults = new ArrayList<>();
        ParsedHTMLResult resultOnlyWithBrokenLinks = new ParsedHTMLResult();
        resultOnlyWithBrokenLinks.setBrokenLinks(initializeBrokenLinks());
        parsedHTMLResults.add(resultOnlyWithBrokenLinks);
        this.markdownGenerator = new MarkdownGenerator(
                parsedHTMLResults,
                CRAWLED_URL,
                CRAWL_DEPTH,
                SOURCE_LANGUAGE,
                TARGET_LANGUAGE,
                USE_TRANSLATOR
        );
    }

    private void initializeMarkdownGeneratorWithFullInformation() {
        ArrayList<ParsedHTMLResult> parsedHTMLResults = new ArrayList<>();
        parsedHTMLResults.add(generateFullyParsedHTML());
        this.markdownGenerator = new MarkdownGenerator(
                parsedHTMLResults,
                CRAWLED_URL,
                CRAWL_DEPTH,
                SOURCE_LANGUAGE,
                TARGET_LANGUAGE,
                USE_TRANSLATOR
        );
    }

    private ParsedHTMLResult generateFullyParsedHTML() {
        ParsedHTMLResult fullyParsedHTML = new ParsedHTMLResult();
        fullyParsedHTML.setHeadings(initializeHeadings());
        fullyParsedHTML.setWorkingLinks(initializeLinks());
        fullyParsedHTML.setBrokenLinks(initializeBrokenLinks());
        return fullyParsedHTML;
    }

    private void initializeMarkdownGeneratorWithExtendedArrowSize() {
        ArrayList<ParsedHTMLResult> parsedHTMLResults = new ArrayList<>();
        ParsedHTMLResult fullyParsedHTML = generateFullyParsedHTML();
        fullyParsedHTML.setCurrentDepth(2);
        parsedHTMLResults.add(fullyParsedHTML);
        this.markdownGenerator = new MarkdownGenerator(
                parsedHTMLResults,
                CRAWLED_URL,
                CRAWL_DEPTH,
                SOURCE_LANGUAGE,
                TARGET_LANGUAGE,
                USE_TRANSLATOR
        );
    }

    private Elements initializeHeadings() {
        Elements headings = new Elements();
        headings.add(new Element("h1").text("Heading1"));
        return headings;
    }

    private Elements initializeLinks() {
        Elements links = new Elements();
        links.add(new Element("a").attr("href", "www.test.com"));
        return links;
    }

    private Elements initializeBrokenLinks() {
        Elements brokenLinks = new Elements();
        brokenLinks.add(new Element("a").attr("href", "www.broken.com"));
        return brokenLinks;
    }

    private String getTestDataFile(String path) throws IOException {
        InputStream inputStream = this.getClass().getResourceAsStream(TEST_DATA_DIRECTORY + path);
        String testData = new String(inputStream.readAllBytes(), StandardCharsets.UTF_8);
        testData = testData.replace("\r", "");
        return testData;
    }

    @Test
    public void testGenerateEmptyMarkdown() throws IOException {
        initializeEmptyMarkdownGenerator();
        String emptyMarkdown = this.markdownGenerator.generateMarkdownString();
        String emptyMarkdownTestData = getTestDataFile("emptyMarkdown");
        Assertions.assertEquals(emptyMarkdownTestData, emptyMarkdown);
    }

    @Test
    public void testGenerateHeadingsOnlyMarkdown() throws IOException {
        initializeMarkdownGeneratorWithHeadingsOnly();
        String headingsOnlyMarkdown = this.markdownGenerator.generateMarkdownString();
        String headingsOnlyTestData = getTestDataFile("headingsOnlyMarkdown");
        Assertions.assertEquals(headingsOnlyTestData, headingsOnlyMarkdown);
    }

    @Test
    public void testGenerateLinksOnlyMarkdown() throws IOException {
        initializeMarkdownGeneratorWithLinksOnly();
        String linksOnlyMarkdown = this.markdownGenerator.generateMarkdownString();
        String linksOnlyTestData = getTestDataFile("linksOnlyMarkdown");
        Assertions.assertEquals(linksOnlyTestData, linksOnlyMarkdown);
    }

    @Test
    public void testGenerateBrokenLinksOnlyMarkdown() throws IOException {
        initializeMarkdownGeneratorWithBrokenLinksOnly();
        String brokenLinksOnlyMarkdown = this.markdownGenerator.generateMarkdownString();
        String brokenLinksOnlyTestData = getTestDataFile("brokenLinksOnlyMarkdown");
        Assertions.assertEquals(brokenLinksOnlyTestData, brokenLinksOnlyMarkdown);
    }

    @Test
    public void testGenerateMarkdownWithFullInformation() throws IOException {
        initializeMarkdownGeneratorWithFullInformation();
        String fullInformationMarkdown = this.markdownGenerator.generateMarkdownString();
        String fullInformationTestData = getTestDataFile("fullInformationMarkdown");
        Assertions.assertEquals(fullInformationTestData, fullInformationMarkdown);
    }

    @Test
    public void testGenerateMarkdownWithExtendedArrowSize() throws IOException {
        initializeMarkdownGeneratorWithExtendedArrowSize();
        String fullInformationWithExtendedArrowsMarkdown = this.markdownGenerator.generateMarkdownString();
        String fullInformationWithExtendedArrowsTestData = getTestDataFile("extendedArrowsMarkdown");
        Assertions.assertEquals(fullInformationWithExtendedArrowsTestData, fullInformationWithExtendedArrowsMarkdown);
    }

}
