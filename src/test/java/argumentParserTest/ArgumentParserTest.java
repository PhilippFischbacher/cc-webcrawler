package argumentParserTest;

import argumentParsing.InputArgumentParser;
import argumentParsing.ParsedInputArguments;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

public class ArgumentParserTest {

    private String[] validArguments;
    private String[] invalidArguments;
    private String[] customArguments;
    private ParsedInputArguments parsedInputArguments;
    private InputArgumentParser inputArgumentParser;

    @BeforeEach
    public void initializeValidArgumentsString(){
        validArguments= new String[]{
                "-url",
                "http://histo.io",
                "-depth",
                "2",
                "-source",
                "EN",
                "-target",
                "DE"
        };
    }

    @BeforeEach
    public void initializeInvalidArgumentsString(){
        invalidArguments= new String[]{
                "invalidURLTagDeclaration",
                "invalidLinkExample",
                "invalidDepthTagDeclaration",
                "incalidDepth",
                "invalidSourceLanguageTagDeclaration",
                "invalidLanguage",
                "invalidTargetLanguageTagDeclaration",
                "invalidLanguage",
                "force wrong number of Arguments"
        };
    }

    @BeforeEach
    public void initializeCustomArgumentsString(){
        customArguments=validArguments.clone();
        inputArgumentParser = new InputArgumentParser(customArguments);
    }

    @Test
    public void testParseInputArgumentsValid() {
        Assertions.assertDoesNotThrow(
                () -> inputArgumentParser.parseInputArguments());
    }

    @Test
    public void testParseInputArgumentsInvalid() {
        inputArgumentParser = new InputArgumentParser(new String[]{});
        Assertions.assertThrows(
                Exception.class,
                        () -> inputArgumentParser.parseInputArguments());
    }

    @Test
    public void testAreNumberOfArgumentsValidError(){
        inputArgumentParser = new InputArgumentParser(new String[]{});
        IllegalArgumentException numberOfArgumentsException = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> inputArgumentParser.parseInputArguments());

        String expectedErrorMessage=("The number of arguments is invalid\n" +
                                    "Please make sure to use this format:\n" +
                                    "-url <URL> -depth <depth how far the program crawls> -source <headings' source language> -target <target language for the headings>");

        Assertions.assertEquals(expectedErrorMessage, numberOfArgumentsException.getMessage());
    }

    @Test
    public void testIsURLArgumentMissingError(){
        customArguments[0]=invalidArguments[0];
        inputArgumentParser = new InputArgumentParser(customArguments);

        IllegalArgumentException invalidURLArgumentTagException = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> inputArgumentParser.parseInputArguments());

        String expectedErrorMessage=("Url Input argument is missing");
        Assertions.assertEquals(expectedErrorMessage, invalidURLArgumentTagException.getMessage());
    }

    @Test
    public void testIsURLArgumentValidError(){
        customArguments[1]=invalidArguments[1];
        inputArgumentParser = new InputArgumentParser(customArguments);

        IllegalArgumentException invalidURLArgumentTagException = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> inputArgumentParser.parseInputArguments());

        String expectedErrorMessage=("Url Input argument is broken!");
        Assertions.assertEquals(expectedErrorMessage, invalidURLArgumentTagException.getMessage());
    }

    @Test
    public void testIsDepthArgumentValid_MissingError(){
        customArguments[2]=invalidArguments[2];
        inputArgumentParser = new InputArgumentParser(customArguments);

        IllegalArgumentException invalidDepthTagException = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> inputArgumentParser.parseInputArguments());

        String expectedErrorMessage=("Depth Input argument is missing or invalid");
        Assertions.assertEquals(expectedErrorMessage, invalidDepthTagException.getMessage());
    }

    @Test
    public void testIsDepthArgumentValid_NonIntegerError(){
        customArguments[3]=invalidArguments[3];
        inputArgumentParser = new InputArgumentParser(customArguments);

        IllegalArgumentException nonIntegerInput = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> inputArgumentParser.parseInputArguments());

        String expectedErrorMessage=("Please enter an integer number as depth input");
        Assertions.assertEquals(expectedErrorMessage, nonIntegerInput.getMessage());
    }

    @Test
    public void testIsSourceLanguageTagValidError(){
        customArguments[4]=invalidArguments[4];
        inputArgumentParser = new InputArgumentParser(customArguments);

        IllegalArgumentException invalidSourceLanguageTagException = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> inputArgumentParser.parseInputArguments());

        String expectedErrorMessage=("Source Language Input tag is missing or invalid");
        Assertions.assertEquals(expectedErrorMessage, invalidSourceLanguageTagException.getMessage());
    }

    @Test
    public void testIsSourceLanguageArgumentValidError(){
        customArguments[5]=invalidArguments[5];
        inputArgumentParser = new InputArgumentParser(customArguments);

        IllegalArgumentException invalidSourceLanguageTagException = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> inputArgumentParser.parseInputArguments());

        String expectedErrorMessage=("Source Language Input argument doesn't follow Deepl language conventions");
        Assertions.assertEquals(expectedErrorMessage, invalidSourceLanguageTagException.getMessage());
    }



    @Test
    public void testIsTargetLanguageTagValidError(){
        customArguments[6]=invalidArguments[6];
        inputArgumentParser = new InputArgumentParser(customArguments);

        IllegalArgumentException invalidTargetLanguageTagException = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> inputArgumentParser.parseInputArguments());

        String expectedErrorMessage=("Target Language Input tag is missing or invalid");
        Assertions.assertEquals(expectedErrorMessage, invalidTargetLanguageTagException.getMessage());
    }

    @Test
    public void testIsTargetLanguageArgumentValidError(){
        customArguments[7]=invalidArguments[7];
        inputArgumentParser = new InputArgumentParser(customArguments);

        IllegalArgumentException invalidTargetLanguageTagException = Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> inputArgumentParser.parseInputArguments());

        String expectedErrorMessage=("Target Language Input argument doesn't follow Deepl language conventions");
        Assertions.assertEquals(expectedErrorMessage, invalidTargetLanguageTagException.getMessage());
    }

}
